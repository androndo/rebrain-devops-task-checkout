# Rebrain Devops Task Checkout

Repository for study to basic DevOps principles via practical works.
Contains Nginx default configure for example.

## Getting Started

Let's start it: 
```git clone git@gitlab.rebrainme.com:androndo/rebrain-devops-task-checkout.git```

Enjoy! 😉

### Prerequisites

See and use documentation 📖 :
* [git-scm ](https://git-scm.com/)
* [Nginx](https://nginx.org/en/)


### Installing

Not required

## Running the tests

It does not have a tests yet.

## Deployment

It does not have a deployment plan yet.

## Built With

* [git-scm ](https://git-scm.com/) - distributed version control system.

## Contributing

Please read **CONTRIBUTING.md** later...

## Authors

* **Andrey Kolkov** - student
* **REBRAIN** - DevOps course authors